#ifndef __NEOPATTERN_STATE__
#define __NEOPATTERN_STATE__

#include <ArduinoJson.h>
#include "JsonStruct.h"
#include "utils/print.h"

struct NeoPatternState : public JsonStruct {
    uint pattern = 0;
    uint color= 0;
    uint color2= 0;
    uint totalSteps = 16;
    uint brightness = 64;

    void mapJsonObject(JsonObject& root) {
        root["pattern"] = pattern;
        root["color"] = color;
        root["color2"] = color2;
        root["totalSteps"] = totalSteps;
        root["brightness"] = brightness;
        
    }
    // Map a json object to this struct.
    void fromJsonObject(JsonObject& json){
        if(!verifyJsonObject(json)){
            PRINT_MSG(Serial, "fromJsonObject", "cannot parse JSON");
            valid = 0;
            return;
        }
        color = getIntAttrFromJson(json, "color", color);
        color2 = getIntAttrFromJson(json, "color2", color2);
        pattern = getIntAttrFromJson(json, "pattern", pattern);
        brightness = getIntAttrFromJson(json, "brightness", brightness);
        totalSteps = getIntAttrFromJson(json, "totalSteps", totalSteps);
        valid = 1;
    };
};

#endif