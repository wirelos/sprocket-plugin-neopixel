#include "config.h"
#include "Sprocket.h"
#include "PixelPlugin.h"

Sprocket *sprocket;

void setup()
{
    sprocket = new Sprocket(
        {STARTUP_DELAY, SERIAL_BAUD_RATE});
    sprocket->addPlugin(new PixelPlugin(
        {LED_STRIP_PIN,
         LED_STRIP_LENGTH,
         LED_STRIP_BRIGHTNESS,
         LED_STRIP_UPDATE_INTERVAL}));
    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}