#include "PixelPlugin.h"

PixelPlugin::PixelPlugin(NeoPattern *neoPattern)
{
    pixels = neoPattern;
    loadConfigFromFile();
    applyConfig(pixelConfig);
    defaultAnimation();
}
PixelPlugin::PixelPlugin(PixelConfig cfg)
{
    pixelConfig.brightness = cfg.brightness;
    pixelConfig.pin = cfg.pin;
    pixelConfig.length = cfg.length;
    pixelConfig.updateInterval = cfg.updateInterval;
    loadConfigFromFile();
    pixels = new NeoPattern(pixelConfig.length, pixelConfig.pin, NEO_GRB + NEO_KHZ800);
    applyConfig(pixelConfig);
    defaultAnimation();
}
PixelPlugin::PixelPlugin()
{
    loadConfigFromFile();
    pixels = new NeoPattern(pixelConfig.length, pixelConfig.pin, NEO_GRB + NEO_KHZ800);
    applyConfig(pixelConfig);
    defaultAnimation();
}

void PixelPlugin::loadConfigFromFile()
{
    if (SPIFFS.begin())
    {
        pixelConfig.fromFile(PIXEL_CONFIG_FILE);
    }
}

void PixelPlugin::applyConfig(NeoPixelConfig cfg)
{
    pixels->setBrightness(pixelConfig.brightness);
}

void PixelPlugin::defaultAnimation()
{
    pixels->RainbowCycle(pixelConfig.updateInterval);
    animate();
}

void PixelPlugin::activate(Scheduler *userScheduler)
{
    animation.set(TASK_MILLISECOND * pixelConfig.updateInterval, TASK_FOREVER, bind(&PixelPlugin::animate, this));
    userScheduler->addTask(animation);
    animation.enable();

    subscribe("pixels/colorWheel", bind(&PixelPlugin::colorWheel, this, _1));
    subscribe("pixels/colorWheel1", bind(&PixelPlugin::colorWheel1, this, _1));
    subscribe("pixels/colorWheel2", bind(&PixelPlugin::colorWheel2, this, _1));
    subscribe("pixels/color", bind(&PixelPlugin::setColor, this, _1));
    subscribe("pixels/color2", bind(&PixelPlugin::setColor2, this, _1));
    subscribe("pixels/pattern", bind(&PixelPlugin::setPattern, this, _1));
    subscribe("pixels/totalSteps", bind(&PixelPlugin::setTotalSteps, this, _1));
    subscribe("pixels/brightness", bind(&PixelPlugin::setBrightness, this, _1));
    subscribe("pixels/state", bind(&PixelPlugin::setState, this, _1));

    PRINT_MSG(Serial, "PIXELS", "NeoPixels activated");
}

void PixelPlugin::setState(String msg)
{
    PRINT_MSG(Serial, "PIXELS", msg.c_str());
    state.fromJsonString(msg);
    //pixels->setBrightness(state.brightness);
    //pixels->ColorSet(state.color);
    pixels->Index = 0;
    pixels->Color1 = state.color;
    pixels->Color2 = state.color2;
    pixels->TotalSteps = state.totalSteps;
    pixels->ActivePattern = (pattern)state.pattern;
    pixels->Direction = FORWARD;
}

void PixelPlugin::colorWheel(String msg)
{
    int color = pixels->Wheel(atoi(msg.c_str()));
    pixels->Color1 = color;
    pixels->ActivePattern = NONE;
    pixels->ColorSet(color);
}
void PixelPlugin::colorWheel1(String msg)
{
    int color = pixels->Wheel(atoi(msg.c_str()));
    pixels->Color1 = color;
    if (pixels->ActivePattern == NONE)
    {
        pixels->ColorSet(color);
    }
}
void PixelPlugin::colorWheel2(String msg)
{
    pixels->Color2 = pixels->Wheel(atoi(msg.c_str()));
}

void PixelPlugin::setTotalSteps(String msg)
{
    pixels->TotalSteps = atoi(msg.c_str());
}

void PixelPlugin::setBrightness(String msg)
{
    int inVal = atoi(msg.c_str());
    pixels->setBrightness(inVal);
    pixels->show();
}

void PixelPlugin::setColor(String msg)
{
    pixels->ActivePattern = NONE;
    pixels->Color1 = atoi(msg.c_str());
    //if(pixels->ActivePattern == NONE){
    pixels->ColorSet(pixels->Color1);
    //}
}

void PixelPlugin::setColor2(String msg)
{
    pixels->Color2 = atoi(msg.c_str());
}

void PixelPlugin::setPattern(String msg)
{
    pixels->Index = 0;
    pixels->Direction = FORWARD;
    pixels->ActivePattern = (pattern)atoi(msg.c_str());
}

void PixelPlugin::animate()
{
    pixels->Update();
    yield();
}

void PixelPlugin::enable()
{
    animation.enable();
}

void PixelPlugin::disable()
{
    animation.disable();
}