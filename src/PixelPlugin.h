#ifndef __PIXEL_PLUGIN__
#define __PIXEL_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include "TaskSchedulerDeclarations.h"
#include "Plugin.h"
#include "NeoPixelConfig.h"
#include "NeoPatternState.h"
#include "NeoPattern.cpp"

using namespace std;
using namespace std::placeholders;

#ifndef PIXEL_CONFIG_FILE
#define PIXEL_CONFIG_FILE "/pixelConfig.json"
#endif

struct PixelConfig
{
    int pin;
    int length;
    int brightness;
    int updateInterval;
};

class PixelPlugin : public Plugin
{
  private:
    NeoPixelConfig pixelConfig;
    NeoPattern *pixels;
    NeoPatternState state;

  public:
    Task animation;
    PixelPlugin(PixelConfig cfg);
    PixelPlugin(NeoPattern *neoPattern);
    PixelPlugin();
    void loadConfigFromFile();
    void applyConfig(NeoPixelConfig cfg);
    void applyConfigFromFile();
    void activate(Scheduler *userScheduler);
    void defaultAnimation();
    void setState(String msg);
    void colorWheel(String msg);
    void colorWheel1(String msg);
    void colorWheel2(String msg);
    void setTotalSteps(String msg);
    void setBrightness(String msg);
    void setColor(String msg);
    void setColor2(String msg);
    void setPattern(String msg);
    void animate();
    void enable();
    void disable();
};

#endif