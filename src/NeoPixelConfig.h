#ifndef __NEOPIXEL_CONFIG__
#define __NEOPIXEL_CONFIG__

#include <ArduinoJson.h>
#include "JsonStruct.h"

struct NeoPixelConfig : public JsonStruct
{
    // FIXME constants!
    int pin = 4;
    int length = 8;
    int brightness = 100;
    int updateInterval = 100;
    int defaultColor = 100; // FIXME remove unused
    void mapJsonObject(JsonObject &root)
    {
        root["pin"] = pin;
        root["length"] = length;
        root["brightness"] = brightness;
        root["updateInterval"] = updateInterval;
        root["defaultColor"] = defaultColor;
    }
    void fromJsonObject(JsonObject &json)
    {
        pin = getIntAttrFromJson(json, "pin", pin);
        length = getIntAttrFromJson(json, "length", length);
        brightness = getIntAttrFromJson(json, "brightness", brightness);
        updateInterval = getIntAttrFromJson(json, "updateInterval", updateInterval);
        defaultColor = getIntAttrFromJson(json, "defaultColor", defaultColor);
    }
};

#endif